### PerfAnalytics

------------


**Project diagram:**

![](https://i.ibb.co/NjkZ3mQ/Ekran-Resmi-2020-02-16-15-45-36.png)


# 1. PerfAnalytics.Js

### basic-perf-analytics

A website performance analyzer.

This library measures this values :

| Measurement  | Description  |
| ------------ | ------------ |
| ttfb  | Time to first byte  |
| fcp  | First contentful paint' start time |
| domContentLoaded  |  Dom load time |
| windowLoad  | Page load time  |
| scriptTotalDurationTime  | Completion of loading script - Beginning of loading script |
|  imageTotalDurationTime | Completion of loading image - Beginning of loading image |
| cssTotalDurationTime  |  Completion of loading css - Beginning of loading css |
| documentTotalDurationTime  | Completion of loading document - Beginning of loading documen |
| fontTotalDurationTime  | Completion of loading font - Beginning of loading font |

**Minified Size:** 1.4 kb

**Minzipped Size:** 684 b

**Size in browser:** 1.51kb

**Dependencies:** 0


### Steps for usage

1. install package

`npm install basic-perf-analytics` or  you can link with :`https://bundle.run/basic-perf-analytics@latest`

2. Usage

You can call the function everywhere.If Dom is created, it gets the variables from browser.Otherwise If Dom isn't created, it waits dom to load.

```javascript
import perfAnalytic from 'basic-perf-analytics'
perfAnalytic();
```

3.Watch the performace test

Performance Dashboard: https://jolly-kilby-11041e.netlify.com/

Example Demo: https://stoic-ptolemy-29ee6b.netlify.com/

Example Usage Website Source Code: https://github.com/hamzaolak/datocms-Snipcart-Gatsby-Example-demo/blob/master/gatsby-browser.js

> Hint: I used cloud gatsby for import my library to a static site 


NPM Url : https://www.npmjs.com/package/basic-perf-analytics

Source code of library : https://gitlab.com/hamzaolak/perf-analytics-library

# 2. PerfAnalytics.Api

Swagger Url: https://app.swaggerhub.com/apis/Kitchen-Member/perf-analytics-api/1.0.0

Demo Url: https://perf-analytics.herokuapp.com/api/perfAnalytic/

Source code of Api : https://gitlab.com/hamzaolak/perf-analytics-api

Note1: I did not add the .env file to .gitignore because you can see the mongo cloud s user name and password.

Note2: I used husky for pre commit and pre push.If you can run test with `npm run test` or `yarn run test`

>  Hint: if Project run with docker-compose, It works faster because mongodb run with node in same network

Usage:

- `git clone https://gitlab.com/hamzaolak/perf-analytics-api`

- `cd perf-analytics-api`

-  `npm start` or `yarn start`

- you can test from http://localhost/api/perfAnalytic/


With Docker: 

- `docker build -t <your username>/node-web-app .`

- `docker run -p 80:80 -d <your username>/node-web-app`

- live in http://localhost/api/perfAnalytic/

With docker-compose: 

- `docker-compose build`

- `docker-compose up`

- if project is run with docker-compose, Mongodb instance run with project

- live in http://localhost/api/perfAnalytic/

# 3. PerfAnalytics.Dashboard

Demo Link: https://jolly-kilby-11041e.netlify.com/

> Hint: when enter the https://stoic-ptolemy-29ee6b.netlify.com/ you can see the performance analytics in demo link

Source code of dashboard: https://gitlab.com/hamzaolak/perf-analytics-dashboard


Usage:

- `git clone https://gitlab.com/hamzaolak/perf-analytics-dashboard`

- `cd perf-analytics-dashboard`

-  `npm start` or `yarn start`

> Hint: if you want to run with backend api.First run the backend after that you can run with `npm run start:dev` or `yarn run start:dev`

- you can test from http://localhost:3000/

Test: 

- used react-testing-library
- run command : `npm run test` or `yarn run test`
- test files are in `__test__` file






